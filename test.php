<?php 
require_once __DIR__ . '/vendor/autoload.php';
$accessToken = 'アクセストークン';

$json_string = file_get_contents('php://input');
$jsonObj = json_decode($json_string);

$replyToken = $jsonObj->{"events"}[0]->{"replyToken"};
$messageId = $jsonObj->{"events"}[0]->{"message"}->{"id"};

//画像ファイルのバイナリ取得
$ch = curl_init("https://api.line.me/v2/bot/message/".$messageId."/content");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
 'Content-Type: application/json; charser=UTF-8',
 'Authorization: Bearer ' . $accessToken
 ));
$result = curl_exec($ch);
curl_close($ch);

//画像ファイルの作成  
$fp = fopen('./test.jpg', 'wb');

if ($fp){
    if (flock($fp, LOCK_EX)){
        if (fwrite($fp,  $result ) === FALSE){
            print('ファイル書き込みに失敗しました<br>');
        }else{
            print($data.'をファイルに書き込みました<br>');
        }

        flock($fp, LOCK_UN);
    }else{
        print('ファイルロックに失敗しました<br>');
    }
}

fclose($fp);

//そのまま画像をオウム返しで送信  
 $response_format_text = [
 "type" => "image",
 "originalContentUrl" => "./test.jpg",
 "previewImageUrl" => "./test.jpg"
 ];

$post_data = [
"replyToken" => $replyToken,
"messages" => [$response_format_text]
];
 
$ch = curl_init("https://api.line.me/v2/bot/message/reply");
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
 'Content-Type: application/json; charser=UTF-8',
 'Authorization: Bearer ' . $accessToken
 ));
$result = curl_exec($ch);
curl_close($ch);